import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.swing.JOptionPane;

import greenfoot.*;

/**
 * An abstract class cannot be used to create objects. The main function 
 * of an abstract class is to define the common properties and common 
 * behavior that several classes have. 
 * 
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
public abstract class Board extends World {
    /**
     * Any attribute must be private
     */
    private boolean  placedFigures  = false;
    private Figure   selectedFigure = null;
	private String   scenario       = null;
	private Class<?> scenariosClass = null;

    /**
     * Constructor for objects of Board class that creates a new world with 
     * worldWidth*worldHeightcells with a cell size of cellSize*cellSize pixels
     */
    public Board(int worldWidth, int worldHeight, int cellSize) {
        super(worldWidth, worldHeight, cellSize);
    }

    /**
     * Constructor for objects of Board class that creates a new world with 
     * worldWidth*worldHeightcells with a cell size of cellSize*cellSize pixels
     */
    public Board(int worldWidth, int worldHeight, int cellSize, boolean bounded) {
        super(worldWidth, worldHeight, cellSize, bounded);
    }

    /**
     * It is used by Greenfoot to execute the behavior of  
     * BoardGame object into the game.
     */
    @Override
    public void act() {
        if (placedFigures) {
            if (Greenfoot.isKeyDown("escape")) {
                initialize();
                
            } else if (Greenfoot.isKeyDown("up") && thereIsScenarios()) {
                initialize();
                if (selectScenarioUp()) {
                    placedFigures = true;
                }
                
            } else if (Greenfoot.isKeyDown("down") && thereIsScenarios()) {
                initialize();
                if (selectScenarioDown()) {
                    placedFigures = true;
                }
                
            } else if (Greenfoot.mouseClicked(this)) {
                final int x = Greenfoot.getMouseInfo().getX();
                final int y = Greenfoot.getMouseInfo().getY();
                final Figure figure = getFigure(x, y);

                if (canBeSelected(figure)) {
                    setSelectedFigure(figure);

                } else if (isForbiddenMovement(x, y)) {
                	setRedSquareAt(x, y);
                }
            }

        } else if (Greenfoot.isKeyDown("f2")) {
            initializeBoard();
            placedFigures = true;
            
        } else if (Greenfoot.isKeyDown("f3")) {
            if (selectScenario()) {
                placedFigures = true;
            }
            
        } else if (Greenfoot.isKeyDown("up")) {
            if (selectScenarioUp()) {
                placedFigures = true;
            }
            
        } else if (Greenfoot.isKeyDown("down")) {
            if (selectScenarioDown()) {
                placedFigures = true;
            }
        }
    }

	/**
	 * Set initial game state without figures
	 */
    abstract protected void initializeEmptyBoard();
    
    /**
     * Set initial game state with all figures added to the board
     * in the initial position
     */
    abstract protected void initializeBoard();

    /**
     * Update the color associated to figures that can be 
     * selected in next turn
     */
    abstract protected void updateTurnColor();
    	
    /**
     * Getter of turn color
     * 
     * @return the color associated to figures that can be selected
     */
    abstract protected Color getTurnColor();

    /**
     * Play error message associated with game state.
     */
    abstract protected void playErrorMessage();
    
    /**
     * Getter of selected figure
     * 
     * @return the figure that has been selected
     */
    public Figure getSelectedFigure() {
        return selectedFigure;
    }

    /**
     * Get the figure that is a position of board.
     * 
     * @param x coordinate of figure.
     * @param y coordinate of figure.
     * @return the figure that is a position (x,y).
     */
    public Figure getFigure(int x, int y) {
        return this.oldestActorAt(x, y, Figure.class);
    }

    /**
     * Get the figure name that is a position of board.
     * 
     * @param x coordinate of figure.
     * @param y coordinate of figure.
     * @return the figure name that is a position (x,y).
     */
    public String getFigureName(int x, int y) {
    	Figure figure = getFigure(x,y);
    	if (figure == null) {
    		return null;
    	} else {
    		return figure.getClass().getSimpleName();
    	}
    }

    /**
     * Informs if there is a figure in a position.
     * 
     * @param x coordinate of figure.
     * @param y coordinate of figure.
     * @return true when there is a figure in (x,y)
     */
    public boolean thereIsFigure(int x, int y) {
        return getObjectsAt(x, y, Figure.class).size()==1;
    }

    /**
     * Informs if there is a figure of selected color in a position.
     * 
     * @param x coordinate of figure.
     * @param y coordinate of figure.
     * @param color associated to figure.
     * @return true when there is a figure of selected color in (x,y)
     */
    public boolean thereIsFigure(int x, int y, Color color) {
        return thereIsFigure(x, y)
                 && getObjectsAt(x, y, Figure.class).get(0).getColor().equals(color);
    }

    /**
     * Informs if there is a selected figure.
     * 
     * @return true when there is a selected figure.
     */
    public boolean thereIsSelectedFigure() {
        return this.selectedFigure != null;
    }

    /**
     * Informs if a figure on the board can be selected for next
     * movement.
     * 
     * @param figure that could be moved 
     * @return true when the figure can be selected.
     */
    public boolean canBeSelected(Figure figure) {
        return selectedFigure == null 
                 && figure != null
                 && figure.getColor().equals(getTurnColor());
    }
    
    /**
     * Mark the figure to be moved and mark the squares to which 
     * this figure could move.
     * 
     * @param figure that was selected to move.
     */
    public void setSelectedFigure(Figure figure) {
        final int x = figure.getX();
        final int y = figure.getY();
        removeObjects(getObjects(Square.class));
        selectedFigure = figure;
        new BlueSquare(x, y);
        figure.drawAllowedMovements();
    }

    /**
     * Add red square to board in the position x,y
     * 
     * @param x coordinate to where the red square is added
     * @param y coordinate to where the red square is added
     */
	public void setRedSquareAt(int x, int y) {
    	new RedSquare(x,y);
    }

    /**
     * Add green square to board in the position x,y
     * 
     * @param x coordinate to where the green square is added
     * @param y coordinate to where the green square is added
     */
    public void setGreenSquareAt(int x, int y) {
    	new GreenSquare(x,y);
    }
    
	/**
     * @return matrix[heightWorld][widthWorld] with the figure names placed on the board
	 */
    public String[][] getBoardFigures() {
    	return getBoardFigures(Figure.class);
    }

	/**
     * @return matrix[heightWorld][widthWorld] with the figure names placed on the board
	 */
    public String[][] getBoardFigures(Class<? extends Figure> figureClass) {
    	String[][] boardFigures = new String[getHeight()][getWidth()];
    	for (int row = 0; row < getHeight(); row++){	
        	for (int column = 0; column < getWidth(); column++){
        		boardFigures[row][column] = "";
        	}
    	}
		for (Figure figure: getObjects(figureClass)) {
			boardFigures[figure.getY()][figure.getX()] = figure.getClass().getName();
			int index = boardFigures[figure.getY()][figure.getX()].indexOf('$');
			if (index != -1) {
				boardFigures[figure.getY()][figure.getX()] = boardFigures[figure.getY()][figure.getX()].substring(index+1);
			}
		}
    	return boardFigures;
    }

	/**
     * @return matrix[heightWorld][widthWorld] with the square names placed on the board
	 */
    public String[][] getBoardSquares() {
    	return getBoardSquares(null);
    }

	/**
     * @return matrix[heightWorld][widthWorld] with the square names placed on the board
	 */
    public String[][] getBoardSquares(Color color) {
    	String[][] boardSquares = new String[getHeight()][getWidth()];
    	for (int row = 0; row < getHeight(); row++){	
        	for (int column = 0; column < getWidth(); column++){
        		boardSquares[row][column] = "";
        	}
    	}
		for (Square square: getObjects(Square.class)) {
			if (color == null || square.getColor().equals(color)) {
				boardSquares[square.getY()][square.getX()] = square.getClass().getName();
				int index = boardSquares[square.getY()][square.getX()].indexOf('$');
				if (index != -1) {
					boardSquares[square.getY()][square.getX()] = boardSquares[square.getY()][square.getX()].substring(index+1);
				}
			}
		}
    	return boardSquares;
    }

	/**
	 * An abstract class cannot be used to create objects. The main function 
	 * of an abstract class is to define the common properties and common 
	 * behavior that several classes have. A class that extends Square class 
	 * is used to mark the associated square with a color and a behavior.
	 * 
	 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
	 * @version 1.0
	 */
	protected abstract class Square extends Actor {
	    /**
	     * Constructor for objects of Square class. It is used 
	     * to mark the board squares during a movement.
	     */
	    public Square(int x, int y) {
	        setImage(getColor());
	        addObject(this, x, y);
	    }

	    /**
	     * Every extended class has a associated color
	     * 
	     * @return the color associated with extended class
	     */
	    abstract public Color getColor();

	    /**
	     * Draw and set a image with the color that was  
	     * selected for this square.
	     * 
	     * @param color selected for this square.
	     */
	    private void setImage(Color color) {
	        GreenfootImage image = new GreenfootImage(40,40);
	        image.setColor(color);
	        image.drawRect(2, 2, 40-5, 40-5);
	        image.drawRect(3, 3, 40-7, 40-7);
	        image.drawRect(4, 4, 40-9, 40-9);
	        setImage(image);
	    }
	}
	
	/**
	 * Remove all figures and all squares, and set initial game state
	 */
    private void initialize() {
		placedFigures  = false;
		selectedFigure = null;
		removeObjects(getObjects(Figure.class));
		removeObjects(getObjects(Square.class));
		removeObjects(getObjects(Title.class));
		initializeEmptyBoard();
	}
    
    /**
     * Move the selected figure to another square, removing the 
     * figure that could be there.
     * 
     * @param x coordinate to where the selected figure is going to move.
     * @param y coordinate to where the selected figure is going to move.
     */
    private void moveSelectedFigure(int x, int y) {
        removeObjects(getObjectsAt(x, y, Figure.class));
        selectedFigure.setLocation(x, y);
        selectedFigure = null;          
        removeObjects(getObjects(Square.class));
        updateTurnColor();
    }

    /**
     * Reset selected figure.
     */
    private void setNullSelectedFigure() {
        selectedFigure = null;          
        removeObjects(getObjects(Square.class));
    }

    /**
     * Informs if an unmarked square on the board has been selected.
     * 
     * @param x coordinate of board where the mouse was clicked.
     * @param y coordinate of board where the mouse was clicked.
     * @return true when it is a empty square of board.
     */
    private boolean thereIsUnmarkedSquare(int x, int y) {
        return x>0 && x<9 && y>0 && y<9 
                 && getObjectsAt(x, y, Square.class).size() == 0;
    }

    /**
     * Inform if a movement is forbidden.
     * 
     * @param x coordinate of new position of selected figure.
     * @param y coordinate of new position of selected figure.
     * @return true when the movement is forbidden.
     */
    private boolean isForbiddenMovement(int x, int y) {
        return selectedFigure != null && thereIsUnmarkedSquare(x, y);
    }

 	/**
	 * The RedSquare class is used to mark the square that has the next 
	 * figure to be moved.
	 * 
	 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
	 * @version 1.0
	 */
	private class BlueSquare extends Square {

	    /**
	     * Constructor for objects of Square class. It is used 
	     * to mark the board squares during a movement.
	     */
	    public BlueSquare(int x, int y) {
	        super(x, y);
	    }

	    /**
	     * It is used by Greenfoot to execute the behavior of 
	     * Square object into the game.
	     */
	    @Override
	    public void act() {
	        if (Greenfoot.mouseClicked(this)) {
	            setNullSelectedFigure();
	        }
	    }

	    /**
	     * Its color is blue
	     */
	    @Override
	    public Color getColor() {
	        return Color.BLUE;
	    }

	}
	
	/**
	 * The RedSquare class is used to mark a prohibited square  
	 * that have been chosen.
	 * 
	 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
	 * @version 1.0
	 */
	private class RedSquare extends Square {

	    /**
	     * Constructor for objects of Square class. It is used 
	     * to mark the board squares during a movement.
	     */
	    public RedSquare(int x, int y) {
	        super(x, y);
	        playErrorMessage();
	    }

	    /**
	     * It is used by Greenfoot to execute the behavior of 
	     * Square object into the game.
	     */
	    @Override
	    public void act() {
	        if (Greenfoot.mouseClicked(this)) {
	            playErrorMessage();
	        }
	    }

	    /**
	     * Its color is red
	     */
	    @Override
	    public Color getColor() {
	        return Color.RED;
	    }

	}
	
    /**
	 * The RedSquare class is used to mark a square to which 
	 * the selected figure can be moved.
	 * 
	 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
	 * @version 1.0
	 */
	private class GreenSquare extends Square {

	    /**
	     * Constructor for objects of Square class. It is used 
	     * to mark the board squares during a movement.
	     */
	    public GreenSquare(int x, int y) {
	        super(x, y);
	    }

	    /**
	     * It is used by Greenfoot to execute the behavior of 
	     * Square object into the game.
	     */
	    @Override
	    public void act() {
	        if (Greenfoot.mouseClicked(this)) {
	            moveSelectedFigure(getX(), getY());
	        }
	    }

	    /**
	     * Its color is green
	     */
	    @Override
	    public Color getColor() {
	        return Color.GREEN;
	    }
	}

	private boolean thereIsScenarios() {
		if (scenariosClass != null) {
			return true;

		} else {		
			try {
				scenariosClass = Class.forName("Scenarios");
				return true;
			} catch (ClassNotFoundException e) {
				return false;
			}
		}
	}

	private boolean selectScenario() {
		if (thereIsScenarios()) {
			String[] scenariosList = getScenarios(scenariosClass);
			if (scenariosList.length > 0) {
				scenario = showInputDialog("Select scenario state", "Scenario States", scenariosList, scenariosList[0]);
				if (scenario != null) {
					return executeScenarioMethod(scenariosClass);
				}
			}
			return false;

		} else {
			return false;
		}
	}

	private boolean selectScenarioDown() {
		if (scenario == null) {
			return selectScenario();

		} else if (thereIsScenarios()) {
			String[] scenariosList = getScenarios(scenariosClass);
			for (int k = 0; k < scenariosList.length; k++) {
				if (scenariosList[k].equals(scenario)) {
					scenario = scenariosList[(k + 1) % scenariosList.length];
					return executeScenarioMethod(scenariosClass);
				}
			}
			return false;

		} else {
			return false;
		}
	}

	private boolean selectScenarioUp() {
		if (scenario == null) {
			return selectScenario();

		} else if (thereIsScenarios()) {
			String[] scenariosList = getScenarios(scenariosClass);
			for (int k = 0; k < scenariosList.length; k++) {
				if (scenariosList[k].equals(scenario)) {
					if (k == 0) {
						scenario = scenariosList[scenariosList.length - 1];
					} else {
						scenario = scenariosList[k - 1];
					}
					return executeScenarioMethod(scenariosClass);
				}
			}
			return false;

		} else {
			return false;
		}
	}

	private boolean executeScenarioMethod(Class<?> scenariosClass) {
		try {
			for (Method method : scenariosClass.getDeclaredMethods()) {
				if (method.getName().equals(scenario)) {
					new Title(scenario);
					method.invoke(null, new Object[] { this });
					return true;
				}
			}
			return false;

		} catch (Exception e) {
			return false;
		}
	}

	private String showInputDialog(String message, String title, String[] options, String selected) {
		return (String) JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE, null, options,
				selected);
	}

	private String[] getScenarios(Class<?> scenariosClass) {
		Method[] declaredMethods = scenariosClass.getDeclaredMethods();
		String[] scenarios = new String[declaredMethods.length];
		for (int k = 0; k < declaredMethods.length; k++) {
			scenarios[k] = declaredMethods[k].getName();
		}
		java.util.Arrays.sort(scenarios);
		return scenarios;
	}

    private class Title extends Actor {
        public Title(String title) {
        	GreenfootImage image = new GreenfootImage(420,20);
        	image.setColor(Color.DARK_GRAY);
        	image.drawString(title, 40, 15);
        	setImage(image);
            addObject(this, 4, 0);
        }
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    ///                                   BoardWork                                            ///
    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Returns the color of the cell that is in the point (x, y)
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @return inside color
     */
    public Color getCellColor(int x, int y) {
        return getCellInsideColor(x, y);
    }

    /**
     * Returns the color border of the cell that is in the point (x, y)
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @return border color
     */
    public Color getCellBorderColor(int x, int y) {
        return getBackground().getColorAt(x * getCellSize(),
            y * getCellSize());
    }

    /**
     * Returns the color inside of the cell that is in the point (x, y)
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @return inside color
     */
    public Color getCellInsideColor(int x, int y) {
        if (getCellSize()<3) {
            return getCellBorderColor(x, y);
        } else {
            return getBackground().getColorAt(x * getCellSize() + 1,
                y * getCellSize() + 1);
        }
    }
    
    /**
     * 
     * @param colors
     * @return
     */
    public Color[][] getBackgrounColor(Color ... colors) {
    	Color[][] boardCellsInsideColor = new Color[getHeight()][getWidth()];
    	if (colors.length == 0) {
            for (int x=0; x < getWidth(); x++) {
                for (int y=0; y < getHeight(); y++) {
            		boardCellsInsideColor[y][x] = getCellInsideColor(x, y);
            	}
        	}
            
    	} else {
	        for (int x=0; x < getWidth(); x++) {
	            for_y: for (int y=0; y < getHeight(); y++) {
	            	for (Color color: colors) {
	            		if (getCellInsideColor(x, y).equals(color)) {
	            			boardCellsInsideColor[y][x] = color;
	            			continue for_y;
	            		}
	            	}
	            	boardCellsInsideColor[y][x] = null;
	        	}
	    	}
    	}
    	return boardCellsInsideColor;
    }
    
    /**
     * 
     * @return
     */
    public String[][] getBackgrounColorName() {
    	java.util.ArrayList<String> allGreenfootColors = new java.util.ArrayList<String>();
		try {
	    	Field[] fields = Class.forName("greenfoot.Color").getDeclaredFields();
	    	for (Field field: fields) {
	    		if (field.getDeclaringClass().equals(Color.class)) {
	    			allGreenfootColors.add(field.getName());
	    		}
	    	}
		} catch (SecurityException | ClassNotFoundException e) {
		}
	    return getBackgrounColorName(allGreenfootColors.toArray(new String[0]));
    }
    
    /**
     * 
     * @param colors
     * @return
     */
    public String[][] getBackgrounColorName(String ... colors) {
    	String[][] boardCellsInsideColor = new String[getHeight()][getWidth()];
        for (int x=0; x < getWidth(); x++) {
            for_y: for (int y=0; y < getHeight(); y++) {
            	for (String color: colors) {
            		if (toColor(color).equals(getCellInsideColor(x, y))) {
            			boardCellsInsideColor[y][x] = color;
            			continue for_y;
            		}
            	}
    			boardCellsInsideColor[y][x] = "";
        	}
    	}
    	return boardCellsInsideColor;
    }
    
    /**
     * Sets the color of the cell that is in the point (x, y)
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @param color new inside and border color
     */
    public void setCellColor(int x, int y, Color color) {
        if (color != null) {
            if (getCellSize()==1) {
                getBackground().setColorAt(x, y, color);

            } else {
                Color colorActual = getBackground().getColor();
                getBackground().setColor(color);
                getBackground().fillRect(x * getCellSize(),
                    y * getCellSize(), 
                    getCellSize(), 
                    getCellSize());
                getBackground().setColor(colorActual);
            }
        }
    }

    /**
     * Sets the border and inside color of the cell that is in the point (x, y).
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @param borderColor new border color.
     * @param insideColor new inside color when the cell size is less than three 
     *                    the cell has not any inside and this parameter is ignored
     */
    public void setCellColor(int x, int y, Color borderColor, Color insideColor) {
        if (getCellSize()<3) {
            setCellColor(x, y, borderColor);

        } else if (borderColor == null) {
            setCellColor(x, y, insideColor);

        } else if (insideColor != null){
            Color colorActual = getBackground().getColor();
            getBackground().setColor(insideColor);
            getBackground().fillRect(x * getCellSize() + 1,
                y * getCellSize() + 1, 
                getCellSize() - 2, 
                getCellSize() - 2);
            getBackground().setColor(borderColor);
            getBackground().drawRect(x * getCellSize(),
                y * getCellSize(), 
                getCellSize()-1, 
                getCellSize()-1);
            getBackground().setColor(colorActual);

        } else {
            Color colorActual = getBackground().getColor();
            getBackground().setColor(borderColor);
            getBackground().drawRect(x * getCellSize(),
                y * getCellSize(), 
                getCellSize()-1, 
                getCellSize()-1);
            getBackground().setColor(colorActual);    		
        }
    }

    /**
     * Sets the color of all cells
     * 
     * @param color new color of all cells
     */
    public void setBackground(Color color) {
        setBackground(color, color);
    }

    /**
     * Sets the border and inside color of all cells
     * 
     * @param borderColor new border color of all cell
     * @param insideColor new inside color of all cell
     */
    public void setBackground(String borderColor, String insideColor) {
    	setBackground(toColor(borderColor),toColor(insideColor));
    }

    /**
     * Sets the border and inside color of all cells
     * 
     * @param borderColor new border color of all cell
     * @param insideColor new inside color of all cell
     */
    public void setBackground(Color borderColor, Color insideColor) {
        for (int x=0; x<getWidth(); x++) {
            for (int y=0; y<this.getHeight(); y++) {
                setCellColor(x, y, borderColor, insideColor);    
            }
        }
    }

    /**
     * Set the color of all cells when cellColor has the same size
     * of world. When the size is different, it does nothing
     * 
     * @param cellColor is the matrix with the cells colors 
     */
    public void setBackground(String[][] cellColor) {
        if (cellColor.length == getHeight() && cellColor[0].length == getWidth()) {
            setBackground(0, 0, cellColor);
        }
    }

    /**
     * Set the color of all cells when cellColor has the same size
     * of world. When the size is different, it does nothing
     * 
     * @param cellColor is the matrix with the cells colors 
     */
    public void setBackground(Color[][] cellColor) {
        if (cellColor.length == getHeight() && cellColor[0].length == getWidth()) {
            setBackground(0, 0, cellColor);
        }
    }

    /**
     * Set the color of all cells when cellColor has the same size
     * of world. When the size is different, it does nothing
     * 
     * @param borderColor is the color of border of all cells
     * @param cellColor is the matrix with the cells colors 
     */
    public void setBackground(Color borderColor, String[][] cellColor) {
        if (cellColor.length == getHeight() && cellColor[0].length == getWidth()) {
            setBackground(0, 0, borderColor, cellColor);
        }
    }

    /**
     * Set the color of all cells when cellColor has the same size
     * of world. When the size is different, it does nothing
     * 
     * @param borderColor is the color of border of all cells
     * @param cellColor is the matrix with the cells colors 
     */
    public void setBackground(Color borderColor, Color[][] cellColor) {
        if (cellColor.length == getHeight() && cellColor[0].length == getWidth()) {
            setBackground(0, 0, borderColor, cellColor);
        }
    }

    /**
     * Set the color of region cells that begins in (xLeftUp, yLeftUp)
     * 
     * @param xLeftUp axis of initial cell
     * @param yLeftUp axis of initial cell
     * @param cellColor is the matrix with the colors that are going to be 
     *                  put in the region that starts at (xLeftUp, yLeftUp) 
     */
    public void setBackground(int xLeftUp, int yLeftUp, String[][] cellColor) {
        if (cellColor.length <= getHeight()-yLeftUp && cellColor[0].length <= getWidth()-xLeftUp) {
            for (int x=0; x<cellColor.length; x++) {
                for (int y=0; y<cellColor[0].length; y++) {
                    setCellColor(xLeftUp+y, yLeftUp+x, toColor(cellColor[x][y]));            	
                }
            }
        } 
    }

    /**
     * Set the color of region cells that begins in (xLeftUp, yLeftUp)
     * 
     * @param xLeftUp axis of initial cell
     * @param yLeftUp axis of initial cell
     * @param cellColor is the matrix with the colors that are going to be 
     *                  put in the region that starts at (xLeftUp, yLeftUp) 
     */
    public void setBackground(int xLeftUp, int yLeftUp, Color[][] cellColor) {
        if (cellColor.length <= getHeight()-yLeftUp && cellColor[0].length <= getWidth()-xLeftUp) {
            for (int x=0; x<cellColor.length; x++) {
                for (int y=0; y<cellColor[0].length; y++) {
                    setCellColor(xLeftUp+y, yLeftUp+x, cellColor[x][y]);            	
                }
            }
        } 
    }

    /**
     * Set the color of region cells that begins in (xLeftUp, yLeftUp)
     * 
     * @param xLeftUp axis of initial cell
     * @param yLeftUp axis of initial cell
     * @param borderColor is the color of border that are going to be put 
     *                    in the region that starts at (xLeftUp, yLeftUp)
     * @param cellColor is the matrix with the colors that are going to be 
     *                  put in the region that starts at (xLeftUp, yLeftUp) 
     */
    public void setBackground(int xLeftUp, int yLeftUp, Color borderColor, String[][] cellColor) {
        if (cellColor.length <= getHeight()-yLeftUp && cellColor[0].length <= getWidth()-xLeftUp) {
            for (int x=0; x<cellColor.length; x++) {
                for (int y=0; y<cellColor[0].length; y++) {
                    setCellColor(xLeftUp+y, yLeftUp+x, borderColor, toColor(cellColor[x][y]));            	
                }
            }
        } 
    }	

    /**
     * Set the color of region cells that begins in (xLeftUp, yLeftUp)
     * 
     * @param xLeftUp axis of initial cell
     * @param yLeftUp axis of initial cell
     * @param borderColor is the color of border that are going to be put 
     *                    in the region that starts at (xLeftUp, yLeftUp)
     * @param cellColor is the matrix with the colors that are going to be 
     *                  put in the region that starts at (xLeftUp, yLeftUp) 
     */
    public void setBackground(int xLeftUp, int yLeftUp, Color borderColor, Color[][] cellColor) {
        if (cellColor.length <= getHeight()-yLeftUp && cellColor[0].length <= getWidth()-xLeftUp) {
            for (int x=0; x<cellColor.length; x++) {
                for (int y=0; y<cellColor[0].length; y++) {
                    setCellColor(xLeftUp+y, yLeftUp+x, borderColor, cellColor[x][y]);            	
                }
            }
        } 
    }	

    /**
     * It returns an object of the greenfoot.Color class associated with the 
     * name of the input color. When this color is not any color declared in 
     * greenfoot.Color then return Color.WHITE
     * 
     * @param colorName is the name of color
     * @return greenfoot.Color object associated with colorName or Color.WHITE
     */
    private Color toColor(String colorName) {
        try {
            Field field = Class.forName("greenfoot.Color").getDeclaredField(colorName.toUpperCase());
            return (Color)field.get(null);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Count the actors number of a class added to the world
     * 
     * @param cls is the class of actor
     * @return the actors number of a class added to the world
     */
    public int actorsNumber(Class<? extends Actor> cls) {
        return getObjects(cls).size();
    }

    /**
     * Count the actors number of a class added to the world at x,y cell
     * 
     * @param cls is the class of actor
     * @return the actors number of a class added to the world at x,y cell
     */
    public int actorsNumberAt(int x, int y, Class<? extends Actor> cls) {
        return getObjectsAt(x, y, cls).size();
    }

    /**
     * Find the oldest actor of a class added to the world
     * 
     * @param cls is the class of actor
     * @return the oldest actor or null when there is not any actor of cls class
     */
    public <T extends Actor> T oldestActor(Class<T> cls) {
        return oldestActor(cls, 0);
    }

    /**
     * Find the nth oldest actor of a class added to the world
     * 
     * @param cls is the class of actor
     * @param nth is the order where it was added
     * @return the nth oldest actor or null when there 
     *         is not any nth oldest actor of cls class
     */
    public <T extends Actor> T oldestActor(Class<T> cls, int nth) {
        if (getObjects(cls).size() > nth) {
            return getObjects(cls).get(nth);
        } else {
            return null;
        }
    }

    /**
     * Find the oldest actor of a class added to the world at x,y cell
     * 
     * @param x X-coordinate of the cell to be checked.
     * @param y Y-coordinate of the cell to be checked.
     * @param cls is the class of actor
     * @return the oldest actor at x,y cell or null when there is  
     *         not any actor of cls class at x,y cell
     */
    public <T extends Actor> T oldestActorAt(int x, int y, Class<T> cls) {
        return oldestActorAt(x, y, cls, 0);
    }

    /**
     * Find the nth oldest actor of a class added to the world at x,y cell
     * 
     * @param x X-coordinate of the cell to be checked.
     * @param y Y-coordinate of the cell to be checked.
     * @param cls is the class of actor
     * @param nth is the order where it was added
     * @return the nth oldest actor at x,y cell or null when there  
     *         is not any nth oldest actor of cls class at x,y cell
     */
    public <T extends Actor> T oldestActorAt(int x, int y, Class<T> cls, int nth) {
        if (getObjectsAt(x, y, cls).size() > nth) {
            return getObjectsAt(x, y, cls).get(nth);
        } else {
            return null;
        }
    }
	
}
