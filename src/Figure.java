import greenfoot.Actor;
import greenfoot.Color;
import greenfoot.Greenfoot;

/**
 * An abstract class cannot be used to create objects. The main 
 * function of an abstract class is to define the common properties 
 * and common behavior that several classes have. All the figures 
 * on the board have in common being on the same board. In addition, 
 * they must define the squares to which they could move at any 
 * moment of the game.
 * 
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
public abstract class Figure extends Actor {

    /**
     * It is used by Greenfoot to execute the behavior of  
     * Figure object into the game.
     */
    @Override
    public void act() {
    	Board board = getBoard();
        if (Greenfoot.mouseClicked(this)) {
            if (board.canBeSelected(this)) {
                board.setSelectedFigure(this);

            } else {
                board.setRedSquareAt(getX(), getY());
            }
        }
    }

    /**
     * Getter of board where the figure was added
     * 
     * @return the board where the figure is
     */
    public Board getBoard() {
        return getWorldOfType(Board.class);
    }

    /**
     * Getter of the figure color
     * 
     * @return the color of figure
     */
    abstract public Color getColor();

    /**
     * This method must be defined by the extended class. 
     * Its purpose is to mark the squared where a figure 
     * can be moved.
     */
    abstract public void drawAllowedMovements();
    
    /**
     * Add green square to board in the position x,y
     * 
     * @param x coordinate to where the green square is added
     * @param y coordinate to where the green square is added
     */
    protected void setGreenSquareAt(int x, int y) {
    	getBoard().setGreenSquareAt(x, y);
    }
    
}