import greenfoot.Color;
import greenfoot.Greenfoot;

/**
 * The Ajedrez class defines the world Greenfoot where the game  
 * is programmed.
 * 
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
public class Ajedrez extends Board {
    
    /**
     * Any attribute must be private
     */
    private Color turnColor;

    /**
     * Constructor for objects of Ajedrez class that creates a new world 
     * with 10x10 cells with a cell size of 40x40 pixels. 
     */
    public Ajedrez() {    
        super(10, 10, 40);
        final Color border = Color.BLACK;
        final Color inside = Color.LIGHT_GRAY;
        setBackground(border, inside);
        turnColor = null;
    }

    /**
     * Set initial game state with all figures added to the board
     * in the initial position
     */
    @Override
    protected void initializeBoard(){

    }

	/**
	 * Set initial game state without figures
	 */
    @Override
    protected void initializeEmptyBoard() {

	}
    
    /**
     * Getter of turn color
     * 
     * @return the color associated to figures that can be selected
     */
    @Override
    protected Color getTurnColor() {
    	return turnColor;
    }
    
    /**
     * Update the color associated to figures that can be 
     * selected in next turn
     */
    @Override
    protected void updateTurnColor() {
        
    }

    /**
     * Play error message associated with game state.
     */
    @Override
    protected void playErrorMessage() {

    }

}
