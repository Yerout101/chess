import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.Color;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class AjedrezTest verifies the MainClass class
 * 
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class AjedrezTest {

    /*
     * Common part of the given for all tests 
     */
    Ajedrez board;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        board = new Ajedrez();
    }

    /*
     * The Ajedrez board has 10x10 cells, where each cell 
     * will have a size of 40x40 pixels.
     */
    @Test
    public void testWidth() {
        // Given

        // When

        // Then
        assertEquals(10, board.getWidth());
    }

    /*
     * The Ajedrez board has 10x10 cells, where each cell 
     * will have a size of 40x40 pixels.
     */
    @Test
    public void testHeight() {
        // Given

        // When

        // Then
        assertEquals(10, board.getHeight());
    }

    /*
     * The Ajedrez board has 10x10 cells, where each cell 
     * will have a size of 40x40 pixels.
     */
    @Test
    public void testCellSize() {
        // Given

        // When

        // Then
        assertEquals(40, board.getCellSize());
    }

}
